//
// Created by Paweł Jarosz on 16.01.2025.
//

use nastya_lisp::syntax::Token;

fn main() {
    let value = Token::Boolean(false);
    println!("Hello, world! {}", value);
}
