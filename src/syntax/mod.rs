//
// Created by Paweł Jarosz on 16.01.2025.
//

mod tokenizer;

use core::fmt::Display;
use std::fmt::{Formatter};

pub use tokenizer::Tokenizer;

pub enum Token {
    SExpressionBegin,
    SExpressionEnd,
    Quote,
    Label(String),
    Boolean(bool),
    Integer(i128),
    Floating(f64),
    String(String),
    Eof
}

impl Display for Token {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self {
            Token::SExpressionBegin => write!(f, "Token(SExpressionBegin)"),
            Token::SExpressionEnd => write!(f, "Token(SExpressionEnd)"),
            Token::Quote => write!(f, "Token(Quote)"),
            Token::Label(label) => write!(f, "Token(Label: {})", label),
            Token::Boolean(value) => write!(f, "Token(Boolean: {})", value),
            Token::Integer(value) => write!(f, "Token(Integer: {})", value),
            Token::Floating(value) => write!(f, "Token(Floating: {})", value),
            Token::String(value) => write!(f, "Token(String: \"{}\")", value),
            Token::Eof => write!(f, "Token(Eof)"),
        }
    }
}
