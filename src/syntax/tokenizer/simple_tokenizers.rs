//
// Created by Paweł Jarosz on 16.01.2025.
//

use crate::syntax::Token;

pub trait SimpleTokenizer {
    fn get_token_if_available(&mut self) -> Option<Token>;
}

