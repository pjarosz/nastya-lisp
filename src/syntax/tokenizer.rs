//
// Created by Paweł Jarosz on 16.01.2025.
//

mod simple_tokenizers;

use crate::syntax::Token;

struct ParsingContext {
    start_position: usize,
    end_position: usize
}

pub struct Tokenizer {
    content: Option<str>,
    parsing_context: ParsingContext
}

impl Tokenizer {
    fn new() -> Self {
        Self {
            content: None,
            parsing_context: ParsingContext {
                start_position: 0,
                end_position: 0
            }
        }
    }

    fn tokenize(&mut self) -> Vec<Token> {
        todo!("Implement function for tokenizing")
    }
}